const API_URL = 'https://nodejs-fighters.herokuapp.com/';
const proxyUrl = 'https://cors-anywhere.herokuapp.com/';

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method
  };

  return fetch(proxyUrl + url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi }